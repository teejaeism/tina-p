import {useEffect} from 'react';

import Landing from './components/Landing';
import Resume from './components/Resume';
import Education from './components/Education';
import Gallery from './components/Gallery';
import Footer from './components/Footer';

import city from './images/city.png';
import AOS from 'aos';
import "aos/dist/aos.css";

import './App.css';

import {Row,Col,Container} from 'react-bootstrap';

function App() {

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  
  return (
    <>
    <Container fluid>
     <Landing />
     <Resume /> 
     <Education />
     <Gallery />
     <Footer />
    </Container>
    </>
  );
}

export default App;
