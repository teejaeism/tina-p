import {Row,Col,Card,CardGroup,Button} from 'react-bootstrap'
import city from '../images/city.png';
import medi from '../images/Medicard.jpg';
import pol from '../images/poland.jpg';
import fin from '../images/edited-2.png';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';


export default function Gallery(){
	return (
		<>
			<Row className="my-5 mt-md-0">
				<Col xs={8} md={4} className="mx-auto">
				  <img src={city} className="img-fluid text-center" />     
				  <h1 className="text-center text-white display-4 mt-3">Gallery</h1>
				</Col>
			</Row>
			<Row>
				<Col xs={10} md={8} className="mx-auto">
					<Carousel
					  additionalTransfrom={0}
					  arrows
					  autoPlay
					  autoPlaySpeed={3000}
					  centerMode={false}
					  className="mb-5 mt-2"
					  containerClass="container-with-dots"
					  dotListClass=""
					  draggable
					  focusOnSelect={false}
					  infinite
					  itemClass=""
					  keyBoardControl
					  minimumTouchDrag={80}
					  renderButtonGroupOutside={false}
					  renderDotsOutside={false}
					  responsive={{
					    desktop: {
					      breakpoint: {
					        max: 3000,
					        min: 1024
					      },
					      items: 1,
					      partialVisibilityGutter: 40
					    },
					    mobile: {
					      breakpoint: {
					        max: 464,
					        min: 0
					      },
					      items: 1,
					      partialVisibilityGutter: 30
					    },
					    tablet: {
					      breakpoint: {
					        max: 1024,
					        min: 464
					      },
					      items: 1,
					      partialVisibilityGutter: 30
					    }
					  }}
					  showDots={false}
					  sliderClass=""
					  slidesToSlide={1}
					  swipeable
					>
						<Col xs={12} md={10} className="mx-auto gcards">
							<Card className="mx-2">
							  <Card.Img variant="top" src={medi} />
							  <span className="sub mt-1 ms-auto text-muted px-2">Article by: TEAM ORANGE-October 8, 2018</span>
							  <Card.Body>
							    <Card.Title className="text-center mb-3">Medicard Philippines Expands Head Office</Card.Title>
							    <Card.Text>
							      	Pronove Tai International Property Consultants facilitated the expansion of Medicard Philippines in The World Centre, Makati.
							    </Card.Text>
							  </Card.Body>
							  <Card.Footer>
							    <a href="https://orangemagazine.ph/2018/medicard-philippines-expands-head-office/?fbclid=IwAR1fsqN7MsphufkSflKnXohExzhak144eBfqjV4ep0j2qxhnF0_RuEodcqM" className="btn btn-secondary d-block">Read More</a>
							  </Card.Footer>
							</Card>
						</Col>
						<Col xs={12} md={10} className="mx-auto gcards">
							<Card className="mx-2">
							  <Card.Img variant="top" src={fin} />
							    <span className="sub mt-1 ms-auto text-muted px-2">Article by: ScandAsia - January 7, 2021</span>
							  <Card.Body>
							    <Card.Title className="text-center mb-3">Pronove Tai welcomes the Embassy of Finland back to the Philippines</Card.Title>
							    <Card.Text>
							      	The Republic of Finland has re-opened its diplomatic mission in the country with the appointment of His Excellency Mr. Juha Pyykkö as the Ambassador and Head of Mission to the Philippines.
							    </Card.Text>
							  </Card.Body>
							  <Card.Footer>
							    <a href="https://scandasia.com/pronove-tai-welcomes-the-embassy-of-the-republic-of-finland-to-the-philippines/?fbclid=IwAR0Yn-EzFVp_kMVqz4YhGMbgsXtUDEzFJPSdtooRjeqHV0kXnwm4trTW-U8" className="btn btn-secondary d-block">Read More</a>
							  </Card.Footer>
							</Card>
						</Col>
						<Col xs={12} md={10} className="mx-auto gcards" >
							<Card className="mx-2">
							  <Card.Img variant="top" src={pol} />
							  <span className="sub mt-1 ms-auto text-muted px-2">Article by: TEAM ORANGE-February 27, 2018</span>
							  <Card.Body>
							    <Card.Title className="text-center mb-3">The Embassy of Poland Reopens In The Philippines</Card.Title>
							    <Card.Text>
							      	After 24 years of absence, Pronove Tai International Property Consultants welcomed back the Embassy of the Republic of Poland in the Philippines to the country. 
							    </Card.Text>
							  </Card.Body>
							  <Card.Footer>
							    <a href="https://orangemagazine.ph/2018/embassy-of-poland-returns-to-manila-after-24-years/?fbclid=IwAR2IHQb5de08dQht5QWybjc7H7YPUDSlpNHWtyGCmPLsxR5h5QbdGXYMCHQ" className="btn btn-secondary d-block">Read More</a>
							  </Card.Footer>
							</Card>
						</Col>
					</Carousel>


				</Col>
			</Row>
		</>

		)
}