import {FaPhoneSquare,FaLinkedin,FaFacebookSquare,FaInstagramSquare} from 'react-icons/fa'
import {Col,Row} from 'react-bootstrap'
export default function Footer(){
	return (

		<footer className="my-5">
		<Row className="text-center text-white mt-5">
			<span>Ma. Christina Lucatin</span>
			<span>Licensed Real Estate Broker</span>
			<span>&copy;2022</span>
			<Col xs={6} md={3} className="mx-auto d-flex justify-content-around align-items-center text-white mt-3">
				<a href="https://www.linkedin.com/in/christina-lucatin-4056ab163/" className="text-white ">
					<FaLinkedin size={30}/>
				</a>
				<a href="https://www.facebook.com/iamTINApie26" className="text-white ">
					<FaFacebookSquare size={30}/>					
				</a>
				<a href="https://www.instagram.com/iamtinabellz/" className="text-white ">
					<FaInstagramSquare size={30}/>					
				</a>
			</Col>
		</Row>
		</footer>
	)
}