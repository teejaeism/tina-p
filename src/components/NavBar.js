
import {Nav,Navbar,Container} from 'react-bootstrap'

export default function NavBar() {
    return (
        <Navbar id="navbar1" bg="transparent" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/">Tina Lucatin</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link href="/" className="nav-link">Home</Nav.Link>
                        <Nav.Link href="/products" className="nav-link">Resume</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}