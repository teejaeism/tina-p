import {Row,Col,Card,Accordion} from 'react-bootstrap'
import city from '../images/city.png';
export default function Education(){
	return (

		<>
			<Row className="my-3 mt-md-0">
				<Col xs={8} md={4} className="mx-auto">
				  <img src={city} className="img-fluid text-center"/>     
				  <h1 className="text-center text-white display-4 mt-3">Education and Certification</h1>
				</Col>
			</Row>
			<Row className="my-5">
				<Col xs={12} md={10} className="text-white text-center mx-auto d-flex align-items-center">
					<Row className="text-white text-center mx-auto d-flex align-items-center">
						<Col xs={12} md={4}>
							<Card className="bg-transparent my-4 mx-4">
								<Card.Header className="p-3">
									<Card.Subtitle className="mb-1">March 2011</Card.Subtitle>
									<Card.Subtitle  className="mb-3">
										Escuela de San Lorenzo Ruiz, Parañaque
									</Card.Subtitle>
									<Card.Title>
										Secondary Education
									</Card.Title>
								</Card.Header>
							</Card>
						</Col>
						<Col xs={12} md={4}>
							<Card className="bg-transparent my-4 mx-4">
								<Card.Header className="p-3">
									<Card.Subtitle className="mb-1">March 2007</Card.Subtitle>
									<Card.Subtitle  className="mb-3">
										St. Scholastica’s College, Manila
									</Card.Subtitle>
									<Card.Title>
										Bachelor of Science in Commerce
									</Card.Title>
									<Card.Subtitle>
										Major in Business Management
									</Card.Subtitle>
								</Card.Header>
							</Card>
						</Col>
						<Col xs={12} md={4}>
							<Card className="bg-transparent my-4 mx-4">
								<Card.Header className="p-3">
									<Card.Subtitle className="mb-1">Professional Regulation Commission (PRC)</Card.Subtitle>
									<Card.Subtitle  className="mb-3">		
										License no. 26908
									</Card.Subtitle>
									<Card.Title>
										Real Estate Broker
									</Card.Title>
								</Card.Header>
							</Card>
						</Col>

					</Row>
				</Col>
			</Row>
		</>


	)
}