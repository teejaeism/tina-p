import {Row,Col,Card,Accordion} from 'react-bootstrap'
import city from '../images/city.png';
export default function Resume(){

	return (
			<>
			<Row className="mt-1 mt-md-0">
				<Col xs={8} md={4} className="mx-auto">
				  <img src={city} className="img-fluid text-center" />     
				</Col>
			</Row>
			<h1 className="text-center text-white display-4 mt-3 mb-5">Professional Experience</h1>
			<Row className="mt-4">
				<Col xs={12} md={6} className="text-white text-center mx-auto d-flex flex-column align-items-center justify-content-around">
					<Card className="bg-transparent my-4">
						<Card.Header className="p-3">
							<Card.Subtitle className="mb-2">Sept 2017-2021</Card.Subtitle>
							<Card.Title>
								ACCOUNT MANAGER
							</Card.Title>
							<Card.Subtitle>
								PRONOVE TAI AND ASSOCIATES (PHILS.), INC.
							</Card.Subtitle>
						</Card.Header>
						<Card.Body className=" d-none d-md-block">
							<ul className="text-start">
								<li>
									Responsible for identifying leasing and investment opportunities, effectively manage the
									client and other parties involved while maintaining the company’s quality core values and
									conclude with a successful contract.
								</li>
								<li>
									Satisfy the annual individual revenue budget; 
								</li>
								<li>
									Operate as the lead point of contact for any and all matters specific to clients;
								</li>

								<li>Build and maintain strong, long-lasting client relationships;</li>

								<li>Encode, in a timely manner, information gathered into the company’s proprietary database
								management system;</li>

								<li>Ensure the timely and successful delivery of solutions based on the client’s needs and
								objectives;</li>

								<li>Share, give feedback and provide inputs on ways to improve work procedures, particularly
								those that relate to the duties of the Account Manager;</li>

								<li>Perform miscellaneous and job-related duties as assigned.</li>
							</ul>
						</Card.Body>
						<Accordion className="d-block d-md-none ">
						  <Accordion.Item eventKey="0" className="bg-transparent">
						    <Accordion.Header className="text-center">Responsiblities</Accordion.Header>
						    <Accordion.Body>
						    <ul className="text-start">
						    	<li>
						    		Responsible for identifying leasing and investment opportunities, effectively manage the
						    		client and other parties involved while maintaining the company’s quality core values and
						    		conclude with a successful contract.
						    	</li>
						    	<li>
						    		Satisfy the annual individual revenue budget; 
						    	</li>
						    	<li>
						    		Operate as the lead point of contact for any and all matters specific to clients;
						    	</li>

						    	<li>Build and maintain strong, long-lasting client relationships;</li>

						    	<li>Encode, in a timely manner, information gathered into the company’s proprietary database
						    	management system;</li>

						    	<li>Ensure the timely and successful delivery of solutions based on the client’s needs and
						    	objectives;</li>

						    	<li>Share, give feedback and provide inputs on ways to improve work procedures, particularly
						    	those that relate to the duties of the Account Manager;</li>

						    	<li>Perform miscellaneous and job-related duties as assigned.</li>
						    </ul>
						    </Accordion.Body>
						  </Accordion.Item>
						</Accordion>
					</Card>
					<Card className="bg-transparent my-4">
						<Card.Header className="p-3">
							<Card.Subtitle className="mb-2">Jun 2016-Sept 2017</Card.Subtitle>
							<Card.Title>
								COMPLIANCE AND DOCUMENTATION OFFICER
							</Card.Title>
							<Card.Subtitle>
								PRONOVE TAI AND ASSOCIATES (PHILS.), INC.
							</Card.Subtitle>
						</Card.Header>
						<Card.Body className=" d-none d-md-block">
							<ul className="text-start">
								<li>
									Responsible for tracking the direction and timelines of all Agency projects;
								</li>
								<li>
									In-charge of conducting cycle time analysis for all project milestones;
								</li>
								<li>
									Monitor project plans, manpower engagements, budgets and expenditures using control
									tools such as project scheduling, organization and communication charts;
								</li>
								<li>
									Report to the Senior Manager and CEO on a regular basis to review the progress of all
									projects and assist with establishing continuous improvement plans;
								</li>
								<li>
									Ensure all substantial documents are in accordance with Legal Standards, obtained and
									secured upon project completion;
								</li>
								
								<li>
									Endorse done deal accounts summary report and activity requirements such as contract
									signing, inauguration, ground breaking, top-off ceremonies to the office of the CEO for
									Public Relations implementation;
								</li>
							</ul>
						</Card.Body>
						<Accordion className="d-block d-md-none ">
						  <Accordion.Item eventKey="0" className="bg-transparent">
						    <Accordion.Header className="text-center">Responsiblities</Accordion.Header>
						    <Accordion.Body>
						    <ul className="text-start">
						    	<li>
						    		Responsible for tracking the direction and timelines of all Agency projects;
						    	</li>
						    	<li>
						    		In-charge of conducting cycle time analysis for all project milestones;
						    	</li>
						    	<li>
						    		Monitor project plans, manpower engagements, budgets and expenditures using control
						    		tools such as project scheduling, organization and communication charts;
						    	</li>
						    	<li>
						    		Report to the Senior Manager and CEO on a regular basis to review the progress of all
						    		projects and assist with establishing continuous improvement plans;
						    	</li>
						    	<li>
						    		Ensure all substantial documents are in accordance with Legal Standards, obtained and
						    		secured upon project completion;
						    	</li>
						    	
						    	<li>
						    		Endorse done deal accounts summary report and activity requirements such as contract
						    		signing, inauguration, ground breaking, top-off ceremonies to the office of the CEO for
						    		Public Relations implementation;
						    	</li>
						    </ul>
						    </Accordion.Body>
						  </Accordion.Item>
						</Accordion>
					</Card>
					<Card className="bg-transparent my-4">
						<Card.Header className="p-3">
							<Card.Subtitle className="mb-2">Oct 2012- April 2015</Card.Subtitle>
							<Card.Title>
								SENIOR DOCUMENTATIONS ASSOCIATE
							</Card.Title>
							<Card.Subtitle>
								MEGAWORLD CORPORATION
							</Card.Subtitle>
						</Card.Header>
						<Card.Body className=" d-none d-md-block">
							<ul className="text-start">
								<li>
									Receives and validates Reservation Requirements, Move-in-clearance requirements and
									Payments thru cash deposits, remittances, local checks and foreign checks

								</li>
								<li>
									Monitors Documentation Process until Turnover Stage and Commission requests for the
									Sales Personnel and Real Estate Brokers
								</li>
								<li>
									Supervises work of teammates
								</li>
								<li>
									Training of Sales Personnel for the documentation processes and turnover stage
								</li>
								<li>
									Assists Sales Personnel, Real Estate Brokers and Clients on their inquiries.
								</li>
							</ul>
						</Card.Body>
						<Accordion className="d-block d-md-none ">
						  <Accordion.Item eventKey="0" className="bg-transparent">
						    <Accordion.Header className="text-center">Responsiblities</Accordion.Header>
						    <Accordion.Body>
						    <ul className="text-start">
						    	<li>
						    		Receives and validates Reservation Requirements, Move-in-clearance requirements and
						    		Payments thru cash deposits, remittances, local checks and foreign checks

						    	</li>
						    	<li>
						    		Monitors Documentation Process until Turnover Stage and Commission requests for the
						    		Sales Personnel and Real Estate Brokers
						    	</li>
						    	<li>
						    		Supervises work of teammates
						    	</li>
						    	<li>
						    		Training of Sales Personnel for the documentation processes and turnover stage
						    	</li>
						    	<li>
						    		Assists Sales Personnel, Real Estate Brokers and Clients on their inquiries.
						    	</li>
						    </ul>
						    </Accordion.Body>
						  </Accordion.Item>
						</Accordion>
					</Card>
					<Card className="bg-transparent my-4">
						<Card.Header className="p-3">
							<Card.Subtitle className="mb-2">July 2012-Oct 2012</Card.Subtitle>
							<Card.Title>
								SALES ADMIN ASSISTANT
							</Card.Title>
							<Card.Subtitle>
								AVIDA LAND CORPORATION
							</Card.Subtitle>
						</Card.Header>
						<Card.Body className="d-md-block d-none ">
							<ul className="text-start">
								<li>
									Validates Booking Sales Documents and Payments
								</li>
								<li>
									Prepares Sales Booking reports and monitors documentation progress
								</li>
								<li>
									Supervises work of team mates
								</li>
								<li>
									Assists Sales Operations Assistants and Clients for their documentation inquiries
								</li>
							</ul>
						</Card.Body>
						<Accordion className="d-block d-md-none ">
						  <Accordion.Item eventKey="0" className="bg-transparent">
						    <Accordion.Header className="text-center d-block">Responsiblities</Accordion.Header>
						    <Accordion.Body>
						    <ul className="text-start">
						    	<li>
						    		Validates Booking Sales Documents and Payments
						    	</li>
						    	<li>
						    		Prepares Sales Booking reports and monitors documentation progress
						    	</li>
						    	<li>
						    		Supervises work of team mates
						    	</li>
						    	<li>
						    		Assists Sales Operations Assistants and Clients for their documentation inquiries
						    	</li>
						    </ul>
						    </Accordion.Body>
						  </Accordion.Item>
						</Accordion>
					</Card>
					<Card className="bg-transparent my-4">
						<Card.Header className="p-3">
							<Card.Subtitle className="mb-2">Apr 2011–May 2012</Card.Subtitle>
							<Card.Title>
								SALES MANAGEMENT ASSISTANT
							</Card.Title>
							<Card.Subtitle>
								SM DEVELOPMENT CORPORATION
							</Card.Subtitle>
						</Card.Header>
						<Card.Body className="d-md-block d-none ">
							<ul className="text-start">
								<li>
									Reviews completeness of Sales Reservation Agreement, Buyer's Information Sheet,
								</li>
								<li>
									Schedule of Payment and Documentary Requirements submitted
								</li>
								<li>
									Checking of availability and reservation information thru SAP
								</li>
								<li>
									Ensures proper encoding of buyer's data information
								</li>
								<li>
									Prepares Sales Reservation Reports
								</li>
								<li>
									Assists Sales Personnel and Clients for their documentation inquiries
								</li>

							</ul>
						</Card.Body>
						<Accordion className="d-block d-md-none ">
						  <Accordion.Item eventKey="0" className="bg-transparent">
						    <Accordion.Header className="text-center">Responsiblities</Accordion.Header>
						    <Accordion.Body>
						    <ul className="text-start">
						    	<li>
						    		Reviews completeness of Sales Reservation Agreement, Buyer's Information Sheet,
						    	</li>
						    	<li>
						    		Schedule of Payment and Documentary Requirements submitted
						    	</li>
						    	<li>
						    		Checking of availability and reservation information thru SAP
						    	</li>
						    	<li>
						    		Ensures proper encoding of buyer's data information
						    	</li>
						    	<li>
						    		Prepares Sales Reservation Reports
						    	</li>
						    	<li>
						    		Assists Sales Personnel and Clients for their documentation inquiries
						    	</li>

						    </ul>
						    </Accordion.Body>
						  </Accordion.Item>
						</Accordion>
					</Card>
				</Col>
			</Row>
		</>
		)

}