
import {Row,Col} from 'react-bootstrap';

import tina from '../images/tina.jpg';
import {FaPhoneSquare,FaLinkedin,FaFacebookSquare,FaInstagramSquare} from 'react-icons/fa'
import {GrMail} from 'react-icons/gr'
export default function Landing(){

	return (
		<Row className="section">
			<Col xs={12} md={6} className="mx-auto text-center d-flex flex-column justify-content-center align-items-center text-white text-center">
				<Col xs={8} md={3} className=" mx-auto mx-md-0">
					<img src={tina} className="img-fluid rounded-circle mt-2 mb-2"/>				
				</Col>
				<div className="border border-dark w-100 mt-2 mb-1" ></div>
				<h1 className="display-3 mt-1">Ma. Christina Lucatin</h1>
				<h4>Licensed Real Estate Broker</h4>
				<p className="mb-0">
					<FaPhoneSquare /> 09989583226/09274680615
				</p>
				<p>
					<GrMail /> tinalucatin@gmail.com
				</p>
				<Col xs={6} md={3} className="mx-auto d-flex justify-content-between align-items-center text-white mt-4 mt-md-5">
					<a href="https://www.linkedin.com/in/christina-lucatin-4056ab163/" className="text-white ">
						<FaLinkedin size={30}/>
					</a>
					<a href="https://www.facebook.com/iamTINApie26" className="text-white ">
						<FaFacebookSquare size={30}/>					
					</a>
					<a href="https://www.instagram.com/iamtinabellz/" className="text-white ">
						<FaInstagramSquare size={30}/>					
					</a>
				</Col>
			</Col>
		</Row>
		)

};